FROM openjdk:7-jre
MAINTAINER xxx xxx@163.com
COPY /target/springbootdocker-0.0.1-SNAPSHOT.jar   /springbootdocker.jar
ENTRYPOINT ["java","-jar","/springbootdocker.jar"]